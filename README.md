# pymntiso

Mount an ISO using udisksctl.

Meant to work on OpenBox with Thunar.

A small python script does this [pymntiso](./src/pymntiso)

It is installed to a user's `~/bin` directory as `~/bin/pymntiso`

Then, if the Thunar file `~/.config/Thunar/uca.xml` exists, and does not already have a likewise _**PyMountISO**_ entry an XML entry is injected, and the next time the user opens a new Thunar instance, a right-click menu item should be available to mount .ISO and .IMG files.
