
all:
	@echo "Use 'make install'"

default:
	@echo "Use 'make install'"

help:
	@echo "pymntiso -- mount an iso using udisksctl"
	@echo "Installs a python script to the user bin directory, ~/bin/pymntiso"
	@echo "Designed to be used with Thunar to mount ISO or IMG files from right-click menus."
	@echo "Use 'make install' to install."
	@echo "No uninstall mechanism provided."
	@echo "Remove from Thunar, and then remove ~/bin/pymntiso file."

install:
	cp src/pymntiso ~/bin/pymntiso
	chmod +x ~/bin/pymntiso
	chmod og-rwx ~/bin/pymntiso
	chmod +x util/thunar-install
	./util/thunar-install
	chmod -x util/thunar-install



